#include "CommunicationClient.hpp"

CommunicationClient::CommunicationClient(LogManager *logManager,
					 StreamManager *streamManager,
					 XmlManager *xmlManager) 
{
  this->__logManager = logManager;
  this->__streamManager = streamManager;
  this->__xmlManager = xmlManager;
  this->__ioService = NULL;
  this->__clientThread = NULL;
}

CommunicationClient::~CommunicationClient() 
{
  std::cout << "Done destroy Communication Client" << std::endl;
}

void CommunicationClient::stopClientCommunication()
{
  /*  if (this->__connection->socket().is_open())
    {
      this->__connection->socket().shutdown(boost::asio::ip::tcp::socket::shutdown_send);
      this->__connection->socket().close();
      }*/
  if (this->__clientThread)
    while(this->__clientThread->joinable())
      {
        std::cout << "quitting thread" << std::endl;
	if (this->__clientThread)
	  this->__clientThread->interrupt();
      }
  
  std::cout << "quit thread" << std::endl;
  delete this;
}

void CommunicationClient::newPacket(std::vector<Info> infos)
{
  boost::thread sendPacket(&CommunicationClient::sendPacketClient, this, infos);

  this->__clientThread = &sendPacket;
  sendPacket.join();
}

void CommunicationClient::sendPacketClient(std::vector<Info> infos)
{
  boost::asio::io_service io_service;

  boost::asio::ip::tcp::resolver resolver(io_service);
  boost::asio::ip::tcp::resolver::query query(IP_SERVER, PORT_CLIENT);
  boost::asio::ip::tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);

  this->__connection = new connection(io_service);
  
  boost::asio::async_connect(this->__connection->socket(), 
			     endpoint_iterator, 
			     boost::bind(&CommunicationClient::handleConnect, 
					 this, 
					 boost::asio::placeholders::error, 
					 infos));  
  this->__ioService = &io_service;
  io_service.run();
}

void CommunicationClient::handleConnect(const boost::system::error_code& e, 
		    const std::vector<Info> & infos)	
{
  if (!e)	
    {
      this->__connection->async_write(infos, 
			      boost::bind(&CommunicationClient::handleWrite, 
					  this, 
					  boost::asio::placeholders::error));
    } 
  else 
    {  
      std::cerr << e.message() << std::endl;
    }
}

void CommunicationClient::handleWrite(const boost::system::error_code& e)
{
  boost::array<char, 1909814> buf;
  std::string __contentData;
  std::string __xmlServer = this->__logManager->getHome() + XML_SERVER;

  if (!e) 
    {
      while (1)	
	{
	  boost::system::error_code error;
	  
	  int len = this->__connection->socket().read_some(boost::asio::buffer(buf), error);
	  std::string tmp(buf.data());
	   
	  //std::cout.write(buf.data(), len);	  
	  if (error == boost::asio::error::eof)
	    {
	      this->__streamManager->replaceContentOnFile(__contentData, __xmlServer);
	      this->getIdClientFile();
	      this->__logManager->logEndReceived("DONE");
	      if (this->__connection->socket().is_open())
		{
		  this->__connection->socket().shutdown(boost::asio::ip::tcp::socket::shutdown_send);
		  this->__connection->socket().close();
		}
	      __contentData = "";
	      break;
	    }
	  __contentData = __contentData + tmp;
	}
    } 
  else 
    {
      this->__logManager->logEndReceived("FAIL");
      std::cerr << e.message() << std::endl;
    }
}


void CommunicationClient::getIdClientFile()
{

}
