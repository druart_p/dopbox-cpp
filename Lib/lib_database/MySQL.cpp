#include "MySQL.hpp"

using namespace std;

namespace db 
{
  MySQL::MySQL() {}
  
  MySQL::MySQL(const string & user, const string & password, const string & database)	
  {
    driver = sql::mysql::get_mysql_driver_instance();
    con = driver->connect("tcp://127.0.0.1:3306", user, password);
    con->setSchema(database);
  }
  
  MySQL::~MySQL()	
  {
    delete con;
  }
  
  const string MySQL::getUser(const string & login, const string & password)	
  {
    sql::PreparedStatement *pstmt;
    pstmt = con->prepareStatement("SELECT id FROM user WHERE login=? and password=?");
    pstmt->setString(1, login);
    pstmt->setString(2, password);
    sql::ResultSet * res = pstmt->executeQuery();
    string id = "";
    if (res->next())	
      {
	id = res->getString(1);
      }
    delete res;
    delete pstmt;
    return id;
  }
  
} /* namespace db */
