#ifndef __ARCHIVEMANAGER_HPP__
#define __ARCHIVEMANAGER_HPP__

/* BOOST INCLUDE */
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/stream.hpp>

/* SYSTEM */
#include <limits.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

/* UTILS */
#include "../../Utils/Constante.h"

class ArchiveManager
{
  /* CONSTRUCTOR & DESTRUCTOR */  
public:
  ArchiveManager();
  ~ArchiveManager();

  /* ARCHIVE MANAGER FUNCTION */
public:
  void gzToString(std::string &data_out, const std::string &path_gz);
  void stringToGz(const std::string &data_in, const std::string &path_gz);
};

#endif /* __ARCHIVEMANAGER__ */
