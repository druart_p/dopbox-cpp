#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <string

#ifndef __INFO_HPP__
#define __INFO_HPP__


class Info
{
private:
  std::string	nom;
  std::string	prenom;

private:
friend class boost::serialization::access;
template<class Archive> void serialize(Archive & ar, const unsigned int version)
{
  ar & boost::serialization::make_nvp("options", nom);
  ar & boost::serialization::make_nvp("options", prenom);
}

  /* COSNTRUCTOR */
public:
  Info();
  Info(std::string nom, std::string prenom);
  ~Info();

  /* GETTER & SETTER */
public:
  void setNom(std::string nom);
  void setPrenom(std::string prenom);
  std::string getNom();
  std::string getPrenom();

};

#endif /* __INFO_HPP__ */
