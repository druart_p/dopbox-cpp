
#include "CommunicationServer.hpp"

CommunicationServer::CommunicationServer(LogManager *logManager, 
					 StreamManager *streamManager, 
					 XmlManager *xmlManager) 
{
  this->__logManager = logManager;
  this->__streamManager = streamManager;
  this->__xmlManager = xmlManager;
  this->__archiveManager= new ArchiveManager();
  this->initParserAction();
}

CommunicationServer::~CommunicationServer() 
{
  //this->__acceptor->close();
  //this->__ioService.stop();
  //this->__serverThread.interrupt();
  //this->__serverThread.join();
  this->__logManager->logServer("Server stopping", "SUCCESS");
}

void CommunicationServer::initParserAction()
{
  this->__funcAction[MOVED_TO]      	= &CommunicationServer::receivedMoveToAction; 
  this->__funcAction[MOVED_FROM]     	= &CommunicationServer::receivedMoveFromAction;
  this->__funcAction[DELETE]      	= &CommunicationServer::receivedDeleteAction;
  this->__funcAction[MODIFY]      	= &CommunicationServer::receivedModifyAction;
  this->__funcAction[CREATE]      	= &CommunicationServer::receivedCreateAction;
  this->__funcAction[REFERENTIEL]    	= &CommunicationServer::receivedReferentielAction;
}

void CommunicationServer::launchServer()
{  
  this->__acceptor = new boost::asio::ip::tcp::acceptor(this->__ioService, 
							boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), 
										       boost::lexical_cast<unsigned short>(portServer)));

  connection_ptr new_conn(new connection(this->__acceptor->get_io_service()));
  
  this->__acceptor->async_accept(new_conn->socket(), boost::bind(&CommunicationServer::handleAccept, 
								 this,  
								 boost::asio::placeholders::error, 
								 new_conn));
  
  this->__logManager->logServer("Server launching", "SUCCESS");
  this->__ioService.run();
}

void CommunicationServer::stopServer()
{
  /*  if ( this->__conn->socket().is_open())
    {
      this->__conn->socket().shutdown(boost::asio::ip::tcp::socket::shutdown_send);
      this->__conn->socket().close();
    }
  */
  this->__logManager->logServer("Server stop", "IN PROGRESS");
  this->__acceptor->close();
  this->__ioService.stop();
  delete this->__xmlManager;
  delete this;
}  

void CommunicationServer::handleRead(const boost::system::error_code &e, connection_ptr conn)	
{
   if (!e)	
    {	
      this->__logManager->logServerRead("DONE", "");
      this->parseData(conn);
    }
  else 
    {
      this->__logManager->logServerRead("FAIL", e.message());
    }
}

void CommunicationServer::handleAccept(const boost::system::error_code &e, connection_ptr conn)	
{
  if (!e)	
    {	
      conn->async_read(this->__infos, boost::bind(&CommunicationServer::handleRead, 
						  this, 
						  boost::asio::placeholders::error, 
						  conn));
    }

  connection_ptr new_conn(new connection(this->__acceptor->get_io_service()));
  this->__acceptor->async_accept(new_conn->socket(), boost::bind(&CommunicationServer::handleAccept, 
								 this,  
								 boost::asio::placeholders::error, 
								 new_conn));
}

void CommunicationServer::parseData(connection_ptr conn)
{
  for(Info i : this->__infos)	
    {
      std::map<std::string, Function>::iterator	__it;

      for (__it = this->__funcAction.begin(); __it != this->__funcAction.end(); ++__it)
	if ((*__it).first == i.getAction())
	  {
	    this->__logManager->logServerAction(i.getAction(), "ACCEPT");
	    (this->*__funcAction[i.getAction()])(&i, conn);
	  }
    }
}

void CommunicationServer::receivedMoveToAction(Info *i, connection_ptr conn)
{
  std::string xml = this->__logManager->getHome() + XML_SERVER  + "referentiel_server.xml";
  std::string __pathFileGz = this->__logManager->getHome() + PATH_SERVER  + i->getName() + ".gz";
  std::string __pathFile = this->__logManager->getHome() + PATH_SERVER + i->getName();
  
  
  this->__streamManager->setContentOnFileGZ(i->getContent(), __pathFileGz);
  
  
  if (this->__xmlManager->isReadable(xml))
    { 
      this->__xmlManager->updateReferentielXml(xml, 
					       i->getIdClient(), 
					       i->getName(), 
					       i->getSize(), 
					       i->getDate(), 
					       i->getAction());		
      
    }
  else
    {
      this->__xmlManager->createReferentielXml(xml, 
					       i->getIdClient(), 
					       i->getName(), 
					       i->getSize(), 
					       i->getDate(), 
					       i->getAction());
    }  

 this->__logManager->logServerAction(i->getAction(), "DONE");
}

void CommunicationServer::receivedMoveFromAction(Info *i, connection_ptr conn)
{
  //TODO 
  
 this->__logManager->logServerAction(i->getAction(), "DONE");
}

void CommunicationServer::receivedDeleteAction(Info *i, connection_ptr conn)
{
  std::string __xml = this->__logManager->getHome() + XML_SERVER;
  std::string __pathFileGz = this->__logManager->getHome() + XML_SERVER + i->getName() + ".gz";

  if (this->__xmlManager->isReadable(__xml))
    { 
      this->__xmlManager->deleteElementOnFile(__xml,
					      i->getIdClient(),
					      i->getName());
      this->__logManager->logServerAction("Delete file on server : " + i->getName(), "DONE");
    }

  remove(__pathFileGz.c_str());

 this->__logManager->logServerAction(i->getAction(), "DONE");
}

void CommunicationServer::receivedModifyAction(Info *i, connection_ptr conn)
{
  std::string __xml = this->__logManager->getHome() + XML_SERVER;
  std::string __pathFileGz = this->__logManager->getHome() + PATH_SERVER  + i->getName() + ".gz";
  std::string __pathFile = this->__logManager->getHome() + PATH_SERVER + i->getName();  
  
  this->__streamManager->setContentOnFileGZ(i->getContent(), __pathFileGz);
  this->__logManager->logServerAddFile(__pathFile);
  
  if (this->__xmlManager->isReadable(__xml))
    { 
      this->__xmlManager->modifyReferentielXml(__xml, 
					       i->getIdClient(), 
					       i->getName(), 
					       i->getSize(), 
					       i->getDate(), 
					       i->getAction());		
      
    }

 this->__logManager->logServerAction(i->getAction(), "DONE");
}

void CommunicationServer::receivedCreateAction(Info *i, connection_ptr conn)
{
  std::string __xml = this->__logManager->getHome() + XML_SERVER;
  std::string __pathFileGz = this->__logManager->getHome() + PATH_SERVER  + i->getName() + ".gz";
  std::string __pathFile = this->__logManager->getHome() + PATH_SERVER + i->getName();
  
  
  this->__streamManager->setContentOnFileGZ(i->getContent(), __pathFileGz);
  this->__logManager->logServerAddFile(__pathFile);
  
  if (this->__xmlManager->isReadable(__xml))
    { 
      this->__xmlManager->updateReferentielXml(__xml, 
					       i->getIdClient(), 
					       i->getName(), 
					       i->getSize(), 
					       i->getDate(), 
					       i->getAction());		
      
    }
  else
    {
      this->__xmlManager->createReferentielXml(__xml, 
					       i->getIdClient(), 
					       i->getName(), 
					       i->getSize(), 
					       i->getDate(), 
					       i->getAction());
    }  

 this->__logManager->logServerAction(i->getAction(), "DONE");
}

void CommunicationServer::receivedReferentielAction(Info *i, connection_ptr conn)
{

  std::string __xmlServer = this->__logManager->getHome() + XML_SERVER;
  std::string __pathFileGz = this->__logManager->getHome() + PATH_SERVER  + i->getName() + ".gz";
  std::string __pathFile = this->__logManager->getHome() + PATH_SERVER + i->getName();
  
  
  this->__streamManager->setContentOnFileGZ(i->getContent(), __pathFileGz);
  this->__logManager->logServerAddFile(__pathFile);
  
  if (this->__xmlManager->isReadable(__xmlServer))
    { 
      this->__xmlManager->updateReferentielXml(__xmlServer, 
					       i->getIdClient(), 
					       i->getName(), 
					       i->getSize(), 
					       i->getDate(), 
					       i->getAction());		
      
    }
  else
    {
      this->__xmlManager->createReferentielXml(__xmlServer, 
					       i->getIdClient(), 
					       i->getName(), 
					       i->getSize(), 
					       i->getDate(), 
					       i->getAction());
    }  

  std::string __contentFile = this->__streamManager->getStringFromFile(__xmlServer);

  this->__logManager->logServerAction(i->getAction(), "SENDING");
  conn->socket().send(boost::asio::buffer(__contentFile));
  this->__logManager->logServerAction(i->getAction(), "SEND DONE");
}
