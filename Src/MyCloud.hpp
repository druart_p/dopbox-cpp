#ifndef __MYCLOUD_HPP__
#define __MYCLOUD_HPP__

#include "../Includes/database/DataBaseType.hpp"
#include "../Includes/database/MySQL.hpp"
#include "../Includes/database/SQLite.hpp"

#include "./Manager/Stream/StreamManager.hpp"

#include "./ViewController/Server/Server.hpp"
#include "./ViewController/Client/Client.hpp"

#include "./Utils/Constante.h"

class MyCloud
{
private:
  Server	       	*__server;
  Client	       	*__client;
  StreamManager	       	*__streamManager;

private:
  std::string	       	__type;

public:
  MyCloud();
  MyCloud(const std::string &type);
  virtual ~MyCloud();

private:
  void			initClient();
  void			initServer();
  void			closeClient();
  void			closeServer();
  const std::string	getEnvVariable() const;
};

#endif /* __MYCLOUD__ */
