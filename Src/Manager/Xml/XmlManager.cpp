#include "XmlManager.hpp"

XmlManager::XmlManager() {}

XmlManager::~XmlManager() {}

bool XmlManager::isReadable(const std::string &file)
{
  std::ifstream fichier(file.c_str());
  return !fichier.fail();
}

int XmlManager::createXml(std::string &xml)
{
  int rc=1;
  /* création d'un buffer XML */
  xmlBufferPtr buf = xmlBufferCreate();
  if (buf==NULL)    {
    printf("Erreur lors de la création d'un buffer xml.\n");
    return 1;
  }
  /* création d'un writer XML */
  xmlTextWriterPtr writer = xmlNewTextWriterMemory(buf, 0);
  if (writer == NULL) {
    printf("Erreur lors de la création d'un xml writer\n");
    return 1;
  }
  /* identation */
  xmlTextWriterSetIndent(writer, 1);

  /* création de l'élément files */
  rc = xmlTextWriterStartElement(writer, BAD_CAST "files");
  if (rc < 0) {
    printf("Erreur lors de l'appel de la fonction xmlTextWriterStartElement\n");
    return 1;
  }

  /* fermeture de l'élément files */
  rc = xmlTextWriterEndDocument(writer);
  if (rc < 0) {
    printf("Erreur lors de l'appel de la fonction xmlTextWriterEndDocument\n");
    return 1;
  }

  /* libération */
  xmlFreeTextWriter(writer);

  xml = (const char *) buf->content;

  /* libération */
  xmlBufferFree(buf);

  return 0;
}

int XmlManager::addElementFile(xmlDocPtr &doc, const t_file &file)
{
  std::string xpathExpr( "/files" );

  /* création du contexte xpath */
  xmlXPathContextPtr xpathCtx = xmlXPathNewContext(doc);
  if(xpathCtx == NULL) {
    printf("Erreur lors de l'appel de la fonction xmlXPathNewContext\n");
    return 1;
  }

  /* evaluation de l'expression xpath */
  xmlXPathObjectPtr xpathObj = xmlXPathEvalExpression((const xmlChar*) xpathExpr.c_str(), xpathCtx);
  if(xpathObj == NULL) {
    printf("Erreur lors de l'appel de la fonction xmlXPathEvalExpression\n");
    xmlXPathFreeContext(xpathCtx);
    return 1;
  }

  /* on se place sur le noeud files */
  xmlNode *node = xpathObj->nodesetval->nodeTab[0];

  /* on ajoute le noeud */
  xmlNodePtr fileNode = xmlNewChild(node, NULL, BAD_CAST "file", NULL);
  xmlNewProp(fileNode, BAD_CAST "idClient", BAD_CAST file.get<0>().c_str());
  xmlNewChild(fileNode, NULL, BAD_CAST "name", BAD_CAST file.get<1>().c_str());
  xmlNewChild(fileNode, NULL, BAD_CAST "size", BAD_CAST file.get<2>().c_str());
  xmlNewChild(fileNode, NULL, BAD_CAST "date", BAD_CAST file.get<3>().c_str());
  xmlNewChild(fileNode, NULL, BAD_CAST "action", BAD_CAST file.get<4>().c_str());

  /* libération */
  xmlXPathFreeObject(xpathObj);
  xmlXPathFreeContext(xpathCtx);

  return 0;
}

xmlNodePtr XmlManager::getFileByIdClient(xmlDocPtr doc, const std::string &id)
{
  std::string xpathExpr( "/files/file[@idClient='" + id + "']" );

  xmlXPathInit();
  
  /* création du contexte xpath */
  xmlXPathContextPtr xpathCtx = xmlXPathNewContext(doc);
  
  if(xpathCtx == NULL) 
    {
      printf("Erreur lors de l'appel de la fonction xmlXPathNewContext\n");
      return NULL;
    }

  /* evaluation de l'expression xpath */
  xmlXPathObjectPtr xpathObj = xmlXPathEvalExpression((const xmlChar*) xpathExpr.c_str(), xpathCtx);
  
  if(xpathObj == NULL) 
    {
      printf("Erreur lors de l'appel de la fonction xmlXPathEvalExpression\n");
      xmlXPathFreeContext(xpathCtx);
      return NULL;
    }

  /* on se place sur le noeud contact */
  xmlNode *node = xpathObj->nodesetval->nodeTab[0];

  /* libération */
  xmlXPathFreeObject(xpathObj);
  xmlXPathFreeContext(xpathCtx);

  return node;
}

xmlNodePtr XmlManager::getFileByIdClientAndName(xmlDocPtr doc, 
						const std::string &id, 
						const std::string &name)
{
  std::string xpathExpr( "/files/file[@idClient='" + id + "' and name='" + name + "']");
  //std::string xpathExpr( "/files/file[@idClient='" + id + "' and name='" + name + "']");
  xmlXPathInit();
  
  /* création du contexte xpath */
  xmlXPathContextPtr xpathCtx = xmlXPathNewContext(doc);
  if(xpathCtx == NULL) {
    printf("Erreur lors de l'appel de la fonction xmlXPathNewContext\n");
    return NULL;
  }

  /* evaluation de l'expression xpath */
  xmlXPathObjectPtr xpathObj = xmlXPathEvalExpression((const xmlChar*) xpathExpr.c_str(), xpathCtx);
  if(xpathObj == NULL) 
    {
      printf("Erreur lors de l'appel de la fonction xmlXPathEvalExpression\n");
      xmlXPathFreeContext(xpathCtx);
      return NULL;
    }

  /* on se place sur le noeud contact */
  xmlNode *node = xpathObj->nodesetval->nodeTab[0];

  /* libération */
  xmlXPathFreeObject(xpathObj);
  xmlXPathFreeContext(xpathCtx);

  return node;
}

xmlNodePtr XmlManager::getFileByXpath(xmlDocPtr doc, const std::string &xpathExpr)
{
  xmlXPathInit();

  /* création du contexte xpath */
  xmlXPathContextPtr xpathCtx = xmlXPathNewContext(doc);
  if(xpathCtx == NULL) 
    {
      printf("Erreur lors de l'appel de la fonction xmlXPathNewContext\n");
      return NULL;
  }

  /* evaluation de l'expression xpath */
  xmlXPathObjectPtr xpathObj = xmlXPathEvalExpression((const xmlChar*) xpathExpr.c_str(), xpathCtx);
  
  if(xpathObj == NULL) 
    {
      printf("Erreur lors de l'appel de la fonction xmlXPathEvalExpression\n");
      xmlXPathFreeContext(xpathCtx);
      return NULL;
    }

  /* on se place sur le noeud contact */
  xmlNode *node = xpathObj->nodesetval->nodeTab[0];

  /* libération */
  xmlXPathFreeObject(xpathObj);
  xmlXPathFreeContext(xpathCtx);

  return node;
}

int XmlManager::deleteElementFile(xmlDocPtr &doc, 
				  const std::string &id, 
				  const std::string &name)
{
  xmlNodePtr file_for_delete = this->getFileByIdClientAndName(doc, id.c_str(), name.c_str());

  if (file_for_delete)
    {
      xmlUnlinkNode(file_for_delete);
      xmlFreeNode(file_for_delete);
    }
  
  return 0;
}

void XmlManager::deleteElementOnFile(const std::string &path, 
				     const std::string &id, 
				     const std::string &name)
{
  xmlDocPtr doc = xmlParseFile(path.c_str());
  xmlNodePtr file_for_delete = this->getFileByIdClientAndName(doc, id.c_str(), name.c_str());
  
  if (file_for_delete)
    {
      xmlUnlinkNode(file_for_delete);
      xmlFreeNode(file_for_delete);
    }
  
 xmlFreeDoc(doc); 
}

int XmlManager::replaceElementFile(xmlDocPtr &doc, const t_file &file)
{
  this->deleteElementFile(doc, file.get<0>(), file.get<1>());
  this->addElementFile(doc, file);
  return 0;
}

unsigned long XmlManager::countElementFilesByClient(xmlDocPtr &doc, const std::string &idClient)
{
  unsigned long count=0;

  xmlXPathInit();

  std::string xpathExpr( "/files/file[@idClient='" + idClient + "']" );

  /* création du contexte xpath */
  xmlXPathContextPtr xpathCtx = xmlXPathNewContext(doc);
  if(xpathCtx == NULL) 
    {
      return NULL;
    }

  /* evaluation de l'expression xpath */
  xmlXPathObjectPtr xpathObj = xmlXPathEvalExpression((const xmlChar*) xpathExpr.c_str(), xpathCtx);
  if(xpathObj == NULL) 
    {
      xmlXPathFreeContext(xpathCtx);
      return NULL;
    }

  return xpathObj->nodesetval->nodeNr;
}


void XmlManager::createReferentielXml(const std::string &path, 
				      const std::string &userId, 
				      const std::string &name, 
				      const std::string &size, 
				      const std::string &date, 
				      const std::string &action)
{
  std::string xml;
  this->createXml(xml);

  xmlDocPtr doc = xmlReadMemory( xml.c_str(), xml.size(), "noname.xml", 0, 0 );

  t_file file = boost::make_tuple(userId, name, size, date, action);
  this->addElementFile(doc, file);
  FILE* xml_file;
  std::string xml_fileName( path );
  xml_file = fopen( xml_fileName.c_str(), "w+" );

  xmlDocFormatDump(xml_file, doc, true);
  
  xmlFreeDoc(doc);
  fclose (xml_file);
}

void XmlManager::updateReferentielXml(const std::string &path, 
				      const std::string &userId, 
				      const std::string &name, 
				      const std::string &size, 
				      const std::string &date, 
				      const std::string &action)
{
  std::string xml;

  xmlDocPtr doc = xmlParseFile( path.c_str() );
  
  t_file file = boost::make_tuple(userId, name, size, date, action);
  
  this->addElementFile(doc, file);
  
  FILE* xml_file;
  std::string xml_fileName( path );
  
  xml_file = fopen( xml_fileName.c_str(), "w+" );
  xmlDocFormatDump(xml_file, doc, true);
  
  xmlFreeDoc(doc);
  fclose (xml_file);
}

void XmlManager::modifyReferentielXml(const std::string &path, 
				      const std::string &userId, 
				      const std::string &name, 
				      const std::string &size, 
				      const std::string &date, 
				      const std::string &action)
{
  std::string xml;

  xmlDocPtr doc = xmlParseFile(path.c_str());
  
  t_file file = boost::make_tuple(userId, name, size, date, action);
  
  this->replaceElementFile(doc, file);
  
  FILE* xml_file;
  std::string xml_fileName( path );
  
  xml_file = fopen( xml_fileName.c_str(), "w+" );
  xmlDocFormatDump(xml_file, doc, true);
  
  xmlFreeDoc(doc);
  fclose (xml_file);
}


long XmlManager::countDismatchXml(const std::string &pathXMLClient, 
					 const std::string &pathXMLServer, 
					 const std::string &idClient)
{ 
  long __count;

  if (this->isReadable(pathXMLClient) &&  this->isReadable(pathXMLServer))
    {
      long		__countClient; 
      long		__countServer;
      xmlDocPtr		__fileClient = xmlParseFile(pathXMLClient.c_str());
      xmlDocPtr		__fileServer = xmlParseFile(pathXMLServer.c_str());
      
      __countClient = this->countElementFilesByClient(__fileClient, idClient);
      __countServer = this->countElementFilesByClient(__fileServer, idClient);
      __count = __countServer - __countClient;
    }
  else if (this->isReadable(pathXMLClient) &&  !(this->isReadable(pathXMLServer)))
    {
      long	__countClient;
      xmlDocPtr __fileClient = xmlParseFile(pathXMLClient.c_str());
      
      __countClient = this->countElementFilesByClient(__fileClient, idClient);
      __count = 0 - __countClient;
    }
  else if (!(this->isReadable(pathXMLClient)) &&  this->isReadable(pathXMLServer))
    {
      long	__countServer;
      xmlDocPtr __fileServer = xmlParseFile( pathXMLServer.c_str() );
      
      __countServer = this->countElementFilesByClient(__fileServer, idClient);
      __count = __countServer;
    }
  else
    {
      __count = 0;
    }

  return __count;
}
