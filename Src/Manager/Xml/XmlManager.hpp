#ifndef __XMLMANAGER_HPP__
#define __XMLMANAGER_HPP__

/* BOOST */
#include "boost/tuple/tuple.hpp"

/* SYSTEM */
#include <string>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>

/* XML */ 
#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>
#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

typedef boost::tuple<std::string, 
		     std::string, 
		     std::string, 
		     std::string, 
		     std::string> t_file;

class XmlManager
{
public:
  XmlManager();
  ~XmlManager();

public:
  bool		isReadable(const std::string &file);
  int		createXml(std::string &xml);
  int		addElementFile(xmlDocPtr &doc, const t_file &file);
  xmlNodePtr	getFileByIdClient(xmlDocPtr doc, const std::string &id);
  xmlNodePtr	getFileByIdClientAndName(xmlDocPtr doc, 
					 const std::string &id, 
					 const std::string &name);
  xmlNodePtr	getFileByXpath(xmlDocPtr doc, const std::string &xpathExpr);
  int		deleteElementFile(xmlDocPtr &doc, 
				  const std::string &id, 
				  const std::string &name);
  void		deleteElementOnFile(const std::string &path, 
				     const std::string &id, 
				     const std::string &name);
  int		replaceElementFile(xmlDocPtr &doc, const t_file &file);
  unsigned long	countElementFilesByClient(xmlDocPtr &doc, const std::string &idClient);
  void		createReferentielXml(const std::string &path, 
				     const std::string &userId, 
				     const std::string &name, 
				     const std::string &size, 
				     const std::string &date, 
				     const std::string &action);
  void		updateReferentielXml(const std::string &path, 
				     const std::string &userId, 
				     const std::string &name, 
				     const std::string &size, 
				     const std::string &date, 
				     const std::string &action);
  void		modifyReferentielXml(const std::string &path, 
				      const std::string &userId, 
				      const std::string &name, 
				      const std::string &size, 
				      const std::string &date, 
				      const std::string &action);
  long		countDismatchXml(const std::string &path_xml_client, 
				 const std::string &path_xml_serveur, 
				 const std::string &idClient);
};

#endif /* __XMLMANAGER_HPP__ */
