#ifndef __EXCEPTION_HPP__
#define __EXCEPTION_HPP__

#include <exception>
#include <iostream>
#include <sstream>

class my_exception_argument_number : public std::exception
{
  std::string msg;

public:
  my_exception_argument_number(int ac)
  {
    std::ostringstream oss;
    oss << "Wrong argument number : \"" << ac - 1 << "\"." << std::endl 
	<< "./MyCloud Client for launch client" << std::endl
	<< "./MyCloud Server for launch server";
    this->msg = oss.str();
  }
  virtual ~my_exception_argument_number() throw() {}
  virtual const char * what() const throw()
  {
    return this->msg.c_str();
  }
};

class my_exception_argument_type : public std::exception
{
  std::string msg;

public:
  my_exception_argument_type(const std::string &argument)
  {
    std::ostringstream oss;
    oss << "Wrong argument type : \"" << argument << "\"." << std::endl 
	<< "./MyCloud Client for launch client" << std::endl
	<< "./MyCloud Server for launch server";
    this->msg = oss.str();
  }
  virtual ~my_exception_argument_type() throw() {}
  virtual const char * what() const throw()
  {
    return this->msg.c_str();
  }
};

class my_exception_cmd : public std::exception
{
  std::string msg;

public:
  my_exception_cmd(const std::string &argument)
  {
    std::ostringstream oss;
    oss << "Wrong Command type : \"" << argument << "\"." << std::endl 
	<< "type 'Help' for more informations";
    this->msg = oss.str();
  }
  virtual ~my_exception_cmd() throw() {}
  virtual const char * what() const throw()
  {
    return this->msg.c_str();
  }
};

class my_exception_file : public std::exception
{
  std::string msg;

public:
  my_exception_file(const std::string &file)
  {
    std::ostringstream oss;
    oss << "Cannot open file : \"" << file << "\".";
    this->msg = oss.str();
  }
  virtual ~my_exception_file() throw() {}
  virtual const char * what() const throw()
  {
    return this->msg.c_str();
  }
};

#endif /* __EXCEPTION__ */
