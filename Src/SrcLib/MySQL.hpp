#ifndef MYSQL_H_
#define MYSQL_H_

#include <cppconn/resultset.h>
#include <cppconn/prepared_statement.h>
#include "./../../Includes/mysql/mysql_connection.h"
#include "./../../Includes/mysql/mysql_driver.h"
#include <string>
#include <sstream>

namespace db 
{
  class MySQL 
  {
  private:
    sql::mysql::MySQL_Driver	*driver;
    sql::Connection		*con;

  public:
    MySQL();
    MySQL(const std::string & user, const std::string & password, const std::string & database);
    virtual ~MySQL();

  public:
    const std::string getUser(const std::string & login, const std::string & password);
    void createUser(const std::string & login, const std::string & password);
  };  
}

#endif /* MYSQL_H_ */
