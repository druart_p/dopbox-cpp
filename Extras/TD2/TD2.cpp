/* SYSTEM */
#include <sys/inotify.h>
#include <limits.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

/* BOOST INCLUDE */
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/stream.hpp>

/* DEFINE */
#define MAX_EVENTS	1024
#define BUF_LEN		(MAX_EVENTS * (sizeof (struct inotify_event) + NAME_MAX + 1))

/* DEFINE COLOR */
#define RED		"\033[0;31m"
#define GREEN		"\033[0;32m"
#define YELLOW		"\033[0;33m"
#define BLUE		"\033[0;34m"
#define MAGENTA		"\033[0;35m"
#define CYAN		"\033[0;36m"
#define NCOLOR		"\033[0m"

void	gzToString(std::string & data_out, const std::string & path_gz)
{
  std::stringstream data;
  std::ifstream file(path_gz.c_str(), std::ifstream::binary);
  boost::iostreams::filtering_streambuf< boost::iostreams::input > in;
  in.push(boost::iostreams::gzip_decompressor());
  in.push(file);
  boost::iostreams::copy(in, data);
  data_out = data.str();
}

void	stringToGz(const std::string & data_in, const std::string & path_gz)
{
  std::stringstream data;
  std::ofstream file(path_gz.c_str(), std::ofstream::binary);
  boost::iostreams::filtering_streambuf< boost::iostreams::input > in;
  in.push(boost::iostreams::gzip_compressor());
  data << data_in;
  in.push(data);
  boost::iostreams::copy(in, file);
}


int	main()
{
  int			fd = inotify_init();
  std::string		home = "/home/tab/";
  int			wd = inotify_add_watch(fd, home.c_str(), IN_MOVED_TO | IN_MOVED_FROM | IN_CREATE | IN_DELETE | IN_MODIFY | IN_OPEN | IN_ACCESS | IN_CLOSE_WRITE);
  struct inotify_event	*event;
  int			length = 0;
  char			buffer[BUF_LEN];
  char			*p;

  while (1)
    {
      length = read(fd, buffer, BUF_LEN);
      for (p = buffer; p < buffer + length; )
	{
	  event = (struct inotify_event *) p;
      
	  if ((event->mask & IN_MOVED_TO) && !(event->mask & IN_ISDIR))
	    {
	      std::cout << GREEN << "Fichier " << event->name << " déplacé dans /home/tab/." << NCOLOR << std::endl;
	  
	      std::string filePath = home + "/" + event->name;
	      std::ifstream dataInFile(filePath.c_str());
	      std::stringstream stringInFile;
	      stringInFile << dataInFile.rdbuf();
	      stringToGz(stringInFile.str(), filePath + ".gz");
	    }
	  else if ((event->mask & IN_MOVED_FROM) && !(event->mask & IN_ISDIR))
	    {
	      std::cout << GREEN << "Fichier " << event->name << " déplacé hors de /home/tab/." << NCOLOR << std::endl;

	      std::string filePath = home + "/" + event->name;
	      std::ifstream dataInFile(filePath.c_str());
	      std::stringstream stringInFile;
	      stringInFile << dataInFile.rdbuf();
	      //gzToString(stringInFile.str(), filePath);
	    }
	  else if ((event->mask & IN_CREATE) && !(event->mask & IN_ISDIR))
	    {
	      std::cout << MAGENTA << "Fichier " << event->name << " créer dans /home/tab/." << NCOLOR << std::endl;
	    }
	  else if ((event->mask & IN_DELETE) && !(event->mask & IN_ISDIR))
	    {
	      std::cout << RED << "Fichier " << event->name << " supprimer de /home/tab/." << NCOLOR << std::endl;
	    }
	  else if ((event->mask & IN_MODIFY) && !(event->mask & IN_ISDIR))
	    {
	      std::cout << YELLOW << "Fichier /home/tab/" << event->name << " à était modifié." << NCOLOR << std::endl;
	    }
	  else if ((event->mask & IN_OPEN) && !(event->mask & IN_ISDIR))
	    {
	      std::cout << BLUE << "Fichier " << event->name << " est ouvert." << NCOLOR << std::endl;
	    }
	  else if ((event->mask & IN_ACCESS) && !(event->mask & IN_ISDIR))
	    {
	      std::cout << CYAN << "Fichier " << event->name << " access." << NCOLOR << std::endl;
	    }
	  else if ((event->mask & IN_CLOSE_WRITE) && !(event->mask & IN_ISDIR))
	    {
	      std::cout << BLUE << "Fichier " << event->name << " fermer." << NCOLOR << std::endl;
	    }
	  
	  p += sizeof(struct inotify_event) + event->len;
	}
      sleep(1);
    }
  
  inotify_rm_watch(fd, wd);
  close(fd);
  
  return 0;
}
