#ifndef MYSQL_H_
#define MYSQL_H_

#include <cppconn/resultset.h>
#include <cppconn/prepared_statement.h>
#include <mysql_connection.h>
#include <mysql_driver.h>
#include <string>

namespace db 
{
  class MySQL 
  {
  private:
    sql::mysql::MySQL_Driver	*driver;
    sql::Connection		*con;

  public:
    MySQL();
    MySQL(const std::string & user, const std::string & password, const std::string & database);
    virtual ~MySQL();

  public:
    const std::string getUser(const std::string & login, const std::string & password);
  };  
} /* namespace db */

#endif /* MYSQL_H_ */
