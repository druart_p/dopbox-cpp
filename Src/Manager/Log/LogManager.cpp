#include "LogManager.hpp"

LogManager::LogManager(const std::string &path, const std::string &home, StreamManager *streamManager)
{
  this->__streamManager = streamManager;
  this->__home		= home;
  this->__path		= path;
}

LogManager::~LogManager() {}

const std::string LogManager::getHome() const
{
  return this->__home;
}

const std::string LogManager::getCurrentTime() const
{
  time_t	__rawtime;
  struct tm *	__timeinfo;
  char		__buffer[80];

  time (&__rawtime);
  __timeinfo = localtime(&__rawtime);

  strftime(__buffer,80,"%d-%m-%Y %I:%M:%S",__timeinfo);
  
  std::string __dateTime(__buffer);

  return __dateTime;
}

void LogManager::logMoveToFile(struct inotify_event *event)
{
  std::string __log;
  std::string __event(event->name);

  __log = "[" + this->getCurrentTime() + "] " + "File " + __event + " move to " + this->__home + ".";

  this->__streamManager->setContentOnFile(__log, this->__path);
}

void LogManager::logMoveFromFile(struct inotify_event *event)
{
  std::string __log;
  std::string __event(event->name);

  __log = "[" + this->getCurrentTime() + "] " + "File " + __event + " move from " + this->__home + ".";

  this->__streamManager->setContentOnFile(__log, this->__path);
}

void LogManager::logCreateFile(struct inotify_event *event)
{
  std::string __log;
  std::string __event(event->name);

  __log = "[" + this->getCurrentTime() + "] " + "File " + __event + " create in " + this->__home + ".";

  this->__streamManager->setContentOnFile(__log, this->__path);
}

void LogManager::logDeleteFile(struct inotify_event *event)
{
  std::string __log;
  std::string __event(event->name);

  __log = "[" + this->getCurrentTime() + "] " + "File " + __event + " delete in " + this->__home + ".";

  this->__streamManager->setContentOnFile(__log, this->__path);
}

void LogManager::logModifyFile(struct inotify_event *event)
{
  std::string __log;
  std::string __event(event->name);

  __log = "[" + this->getCurrentTime() + "] " + "File " + __event + " modify in " + this->__home + ".";

  this->__streamManager->setContentOnFile(__log, this->__path);
}

void LogManager::logOpenFile(struct inotify_event *event)
{
  std::string __log;
  std::string __event(event->name);

  __log = "[" + this->getCurrentTime() + "] " + "File " + __event + " open in " + this->__home + ".";

  this->__streamManager->setContentOnFile(__log, this->__path);
}

void LogManager::logAccesFile(struct inotify_event *event)
{
  std::string __log;
  std::string __event(event->name);

  __log = "[" + this->getCurrentTime() + "] " + "File " + __event + " acces in " + this->__home + ".";

  this->__streamManager->setContentOnFile(__log, this->__path);
}

void LogManager::logEndReceived(const std::string &status)
{
  std::string __log;
 
  __log = "[" + this->getCurrentTime() + "] " + "Send to server from " 
	+ this->__home + " [" + status + "].";

  this->__streamManager->setContentOnFile(__log, this->__path);
}

void LogManager::logCmdUser(const std::string &command)
{
  std::string __log;
 
  __log = "[" + this->getCurrentTime() + "] " + "User command " 
	+ " [" + command + "].";

  this->__streamManager->setContentOnFile(__log, this->__path);
}

void LogManager::logCmdUser(const std::string &command, const std::string &status)
{
  std::string __log;
 
  __log = "[" + this->getCurrentTime() + "] " 
	+ command + " [" + status + "].";

  this->__streamManager->setContentOnFile(__log, this->__path);
}


void LogManager::logServerAction(const std::string &action, const std::string &status)
{
  std::string __log;
 
  __log = "[" + this->getCurrentTime() + "] Server action : " 
	+ action + " [" + status + "].";

  this->__streamManager->setContentOnFile(__log, this->__path);
}

void LogManager::logServerAddFile(const std::string &file)
{
  std::string __log;
 
  __log = "[" + this->getCurrentTime() + "] Server add file : [" + file + "].";

  this->__streamManager->setContentOnFile(__log, this->__path);
}

void LogManager::logServer(const std::string &msg, const std::string &status)
{
  std::string __log;
  
  __log = "[" + this->getCurrentTime() + "] Server : " 
    + msg + " [" + status + "].";
  
  this->__streamManager->setContentOnFile(__log, this->__path);
}

void LogManager::logServerRead(const std::string &status, const std::string &error)
{
  std::string __log;
 
  if (error == "")
    {
      __log = "[" + this->getCurrentTime() + "] " + "Server read data : " 
	+ " [" + status + "].";
    }
  else
    {
      __log = "[" + this->getCurrentTime() + "] " + "Server read data : " 
	+ " [" + status + "] : " + error;
    }
      
  this->__streamManager->setContentOnFile(__log, this->__path);
}

void LogManager::logClientSendPacket(const std::string &file)
{
  std::string __log;
 
  __log = "[" + this->getCurrentTime() + 
	+ "] Client send packet info [" + file + "].";

  this->__streamManager->setContentOnFile(__log, this->__path);
}

void LogManager::logClient(const std::string &msg, const std::string &status)
{
  std::string __log;
  
  __log = "[" + this->getCurrentTime() + "] Client : " 
    + msg + " [" + status + "].";
  
  this->__streamManager->setContentOnFile(__log, this->__path);
}
