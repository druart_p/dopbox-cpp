#ifndef SQLITE_H_
#define SQLITE_H_

#include <string>
#include <sqlite3.h>

namespace db 
{
  class SQLite 
  {
  private:
    sqlite3 *db;
  public:
    SQLite();
    SQLite(const std::string & database);
    virtual ~SQLite();

  public:
    const std::string getUser(const std::string & login, const std::string & password);
  };
} /* namespace db */

#endif /* SQLITE_H_ */
