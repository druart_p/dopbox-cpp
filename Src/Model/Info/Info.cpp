#include "Info.hpp"

Info::Info() {}

Info::~Info() {}

const std::string &Info::getPath() const
{
  return this->__path;
}

void Info::setPath(const std::string &path)
{
  this->__path = path;
}

const std::string &Info::getName() const
{
  return this->__name;
}

void Info::setName(const std::string &name)
{
  this->__name = name;
}

const std::string &Info::getSize() const
{
  return this->__size;
}

void Info::setSize(const std::string &size)
{
  this->__size = size;
}
 
const std::string &Info::getContent() const
{
  return this->__content;
}

void Info::setContent(const std::string &content)
{
  this->__content = content;
}

const std::string &Info::getAction() const
{
  return this->__action;
}

void Info::setAction(const std::string &action)
{
  this->__action = action;
}

const std::string &Info::getIdClient() const
{
  return this->__idClient;
}

void Info::setIdClient(const std::string &idClient)
{
  this->__idClient = idClient;
}

const std::string &Info::getDate() const
{
  return this->__date;
}

void Info::setDate(const std::string &date)
{
  this->__date = date;
}
