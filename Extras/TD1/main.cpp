#include <iostream>

#include "src/DataBaseType.hpp"
#include "src/SQlite.hpp"

using namespace std;

int main(int argc, char *argv[])
{
    DataBaseType<SQLite> *test = new DataBaseType<SQLite>("/home/xxxxx/temp/mycloud.sqlite3") ;

    string idSQLite = test->getUser("xxxxx", "xxxxx");

    cout << "Test SQLite" << endl << "idSQLite : " << idSQLite << endl;

    return 0;
}