#ifndef __STREAMMANAGER_HPP__
#define __STREAMMANAGER_HPP__

/* SYSTEM */
#include <limits.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <list>
#include <termios.h>

/* UTILS */
#include "../../Utils/Constante.h"
#include "../../Utils/Exception.hpp"

class StreamManager
{
  /* CONSTRUCTOR & DESTRUCTOR */  
public:
  StreamManager();
  ~StreamManager();

  /* STREAM MANAGER FUNCTION */
public:
  std::string			getStreamFromTerm(bool password, const std::string &prompt);
  void				HideTerminalInput(bool hide);
  std::list<std::string>	getListFromFile(const std::string &file);
  std::string			getStringFromFile(const std::string &file);
  void				setContentsOnFile(std::list<std::string> contents, const std::string &fileName);
  void				setContentOnFile(std::string content, const std::string &fileName);
  void				setContentOnFileGZ(std::string content, const std::string &fileName);
  void				replaceContentOnFile(std::string content, const std::string &fileName);
};

#endif /* __STREAMMANAGER__ */
