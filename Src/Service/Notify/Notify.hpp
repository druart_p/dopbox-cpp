#ifndef __NOTIFY_HPP__
#define __NOTIFY_HPP__

/* BOOST INCLUDE */
#include <boost/thread.hpp>

/* SYSTEM */
#include <sys/inotify.h>
#include <limits.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <map>

/* SERVICE */
#include "./../Client/CommunicationClient.hpp"

/* MANAGER */
#include "./../../Manager/Log/LogManager.hpp"
#include "./../../Manager/Stream/StreamManager.hpp"
#include "./../../Manager/Xml/XmlManager.hpp"
#include "./../../Manager/Archive/ArchiveManager.hpp"

/* UTILS */
#include "../../Utils/Constante.h"

class Notify
{
private:
  std::string		__home;
  std::string		__id;
  std::string		__lastGZ;
  int			__refreshTime;	
  int			__fd;
  int			__wd;
  char			__buffer[BUF_LEN];
  bool			__listenning;

private:
  CommunicationClient	*__communicationClient;
  LogManager		*__logManager;
  StreamManager		*__streamManager;
  XmlManager		*__xmlManager;
  ArchiveManager       	*__archiveManager;
  boost::thread		*__threadNotify;

private:
  typedef void (Notify:: *Function)(struct inotify_event *event);
  std::map<uint32_t, Function>	__funcEvent;
  std::vector<Info>		__infos;
  //  std::vector<boost::thread*>	__threads;

public:
  Notify(int refreshTime, 
	 const std::string &id, 
	 LogManager *logManager, 
	 StreamManager *streamManger);
  virtual ~Notify();

public:
  void			setRefreshTime(int refreshTime);
  const int		getRefreshTime() const;
  void			setListenning(bool listenning); 
  const std::string	getEnvVariable() const;
  
private: 
  void			init();
  void			initEventParser();
  void			initLogParser();
  void			initThreadCommunication();

public:
  void			startNotify();
  void			stopNotify();

private:
  void			notifyEventListener();
  void			eventMoveToFile(struct inotify_event *event);
  void			eventMoveFromFile(struct inotify_event *event);
  void			eventCreateFile(struct inotify_event *event);
  void			eventDeleteFile(struct inotify_event *event);
  void			eventModifyFile(struct inotify_event *event);
  void			eventOpenFile(struct inotify_event *event);
  void			eventAccesFile(struct inotify_event *event);

public:
  void			getReferentielPacket(const std::string &action);

private:
  void			preparePacket(const std::string &file, const std::string &action);
  void			sendDelete(const std::string &file, const std::string &action);
};

#endif /* __NOTIFY__ */
