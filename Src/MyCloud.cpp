#include "MyCloud.hpp"

MyCloud::MyCloud() 
{
  this->__type = "";
}

MyCloud::MyCloud(const std::string &type)
{
  this->__streamManager = new StreamManager(); 
  if (type == client)
    this->initClient();
  else if (type == server)
    this->initServer();
}

MyCloud::~MyCloud()
{
  if (this->__type != "")
    {
      if (this->__type == client)
	this->closeClient();
      else if (this->__type == server)
	this->closeServer();
      delete this->__streamManager; 
    }
}

void MyCloud::initClient()
{
  this->__client = new Client(this->__streamManager, this->getEnvVariable());
}

void MyCloud::initServer()
{
  this->__server = new Server(this->__streamManager, this->getEnvVariable());
}

void MyCloud::closeClient()
{
  delete this->__client;
}

void MyCloud::closeServer()
{
  delete this->__server;
}

const std::string MyCloud::getEnvVariable() const
{
  return getenv("PWD");
}
