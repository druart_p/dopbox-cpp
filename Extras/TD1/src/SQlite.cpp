#include "SQLite.hh"

/* Constructor */

SQLite::SQLite()
{

}

SQLite::SQLite(const std::string & database)
{
    if(sqlite3_open(database.c_str(), &this->db))
    {
        throw std::string("Impossible d'ouvrir la base de données.");
    }
}

SQLite::~SQLite()
{
    sqlite3_close(this->db);
}

const std::string SQLite::getUser(const std::string & login, const std::string & password)
{
    sqlite3_stmt *stmt;
    std::string query = "SELECT id FROM user WHERE login=? and password=?";

    if ( sqlite3_prepare(this->db, query.c_str(), query.size(), &stmt, NULL ) != SQLITE_OK)
    {
        throw std::string("Erreur lors de l'appel de la fonction sqlite3_prepare.");
    }

    if (sqlite3_bind_text(stmt, 1, login.c_str(), login.size(),0) != SQLITE_OK)
    {
        throw std::string("Erreur lors de l'appel de la fonction sqlite3_bind_text.");
    }

    if (sqlite3_bind_text(stmt, 2, password.c_str(), password.size(),0) != SQLITE_OK)
    {
        throw std::string("Erreur lors de l'appel de la fonction sqlite3_bind_text.");
    }

    sqlite3_step( stmt );

    std::string id = (char*) sqlite3_column_text( stmt, 0 );

    sqlite3_finalize(stmt);

    return id;
}
