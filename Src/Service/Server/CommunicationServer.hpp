#ifndef __COMMUNICATIONSERVER_HPP__
#define __COMMUNICATIONSERVER_HPP__

/* BOOST */
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>
#include <boost/date_time.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>

/* SYSTEM */
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>

/* MODEL */
#include "./../../Model/Info/Info.hpp"

/* SERVICE */
#include "./../../Service/Connection/connection.hpp"

/* MANAGER */
#include "./../../Manager/Log/LogManager.hpp"
#include "./../../Manager/Stream/StreamManager.hpp"
#include "./../../Manager/Xml/XmlManager.hpp"
#include "./../../Manager/Archive/ArchiveManager.hpp"

/* UTILS */
#include "./../../Utils/Constante.h"

class CommunicationServer
{
private:
  LogManager				*__logManager;
  StreamManager				*__streamManager;
  XmlManager				*__xmlManager;
  ArchiveManager       			*__archiveManager;

private:
  std::vector<Info>			__infos;

private:
  typedef void (CommunicationServer:: *Function)(Info *info, connection_ptr conn);
  std::map<std::string, Function>	__funcAction;

private:
  connection_ptr	       	        __conn;
  boost::asio::ip::tcp::acceptor	*__acceptor;
  boost::asio::io_service		__ioService;
  boost::thread				*__serverThread;

public:
  CommunicationServer(LogManager *logManager, 
		      StreamManager *streamManager, 
		      XmlManager *xmlManager);
  ~CommunicationServer();

public:
  void launchServer();
  void stopServer();

private:
  void handleAccept(const boost::system::error_code& e, connection_ptr conn);
  void handleRead(const boost::system::error_code& e, connection_ptr conn);

private:
  //  void saveData();
  void initParserAction();
  void parseData(connection_ptr conn);

private:
  void receivedMoveToAction(Info *i, connection_ptr conn);
  void receivedMoveFromAction(Info *i, connection_ptr conn);
  void receivedDeleteAction(Info *i, connection_ptr conn);
  void receivedModifyAction(Info *i, connection_ptr conn);
  void receivedCreateAction(Info *i, connection_ptr conn);
  void receivedReferentielAction(Info *i, connection_ptr conn);
};

#endif /* __COMMUNICATIONSERVER__ */
