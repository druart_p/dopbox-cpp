#ifndef __COMMUNICATIONCLIENT_HPP__
#define __COMMUNICATIONCLIENT_HPP__

/* BOOST */
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>

/* SYSTEM */
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <ctime>
#include <stdio.h>

/* MODEL */
#include "./../../Model/Info/Info.hpp"

/* SERVICE */
#include "./../../Service/Connection/connection.hpp"

/* MANAGER */
#include "./../../Manager/Log/LogManager.hpp"
#include "./../../Manager/Stream/StreamManager.hpp"
#include "./../../Manager/Xml/XmlManager.hpp"

/* UTILS */
#include "./../../Utils/Constante.h"

typedef boost::shared_ptr<connection> connection_ptr;

class CommunicationClient
{
private:
  connection	*__connection;
  boost::thread	*__clientThread;
  LogManager	*__logManager;
  StreamManager	*__streamManager;
  XmlManager	*__xmlManager;
  boost::asio::io_service *__ioService;
  
public:
  CommunicationClient(LogManager *logManager, 
		      StreamManager *streamManager, 
		      XmlManager *xmlManager);
  ~CommunicationClient();

public:
  void newPacket(std::vector<Info> infos);
  void preparePacket(const std::string &file);
  void updateReferentiel();
  void stopClientCommunication();

private:
  void sendPacketClient(std::vector<Info> infos);
  void handleWrite(const boost::system::error_code& e);
  void handleConnect(const boost::system::error_code& e, 
		     const std::vector<Info> & infos);
private:
  void getIdClientFile();
};

#endif /* __COMMUNICATIONCLIENT__ */
