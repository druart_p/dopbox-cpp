#ifndef __SERVER_HPP__
#define __SERVER_HPP__

/* BOOST */
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>
#include <boost/date_time.hpp>

/* SYSTEM */
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>

/* MANAGER */
#include "./../../Manager/Stream/StreamManager.hpp"
#include "./../../Manager/Log/LogManager.hpp"

/* MODEL */
#include "./../../Model/Info/Info.hpp"

/* SERVICE */
#include "./../../Service/Server/CommunicationServer.hpp"

/* UTILS */
#include "./../../Utils/Constante.h"

class Server
{
private:
  StreamManager				*__streamManager;
  CommunicationServer			*__communicationServer;
  LogManager				*__logManager;

private:
  boost::asio::ip::tcp::acceptor	*__acceptor;
  boost::thread				*__serverThread;
  std::vector<Info>			__infos;
  std::string				__lastCmd;

private:
  typedef void (Server:: *Function)();
  std::map<std::string, Function>	__func;

public:
  Server(StreamManager *streamManager, const std::string &home);
  ~Server();

private:
  void serverEventListener();

private:
  void initEventParser();
  void eventHelpServer();
  void eventStatusServer();
  void eventStartServer();
  void eventStopServer();
  void eventLogServer();
  void eventExitServer();
  void eventCmdNotFound(const std::string &cmd);
};

#endif /* __SERVER__ */
