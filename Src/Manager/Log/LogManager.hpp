#ifndef __LOGMANAGER_HPP__
#define __LOGMANAGER_HPP__

/* BOOST INCLUDE */
#include <boost/thread.hpp>

/* SYSTEM */
#include <sys/inotify.h>
#include <limits.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <ctime>

/* MANAGER */
#include "./../Stream/StreamManager.hpp"
#include "./../Log/LogManager.hpp"

/* UTILS */
#include "../../Utils/Constante.h"

class LogManager
{
private:
  StreamManager		*__streamManager;

private:
  std::string    	__home;
  std::string		__path;

public:
  LogManager(const std::string &path, const std::string &home, StreamManager *streamManager);
  ~LogManager();

public:
  const std::string	getCurrentTime() const;	
  const std::string	getHome() const;

public:
  void			logMoveToFile(struct inotify_event *event);
  void			logMoveFromFile(struct inotify_event *event);
  void			logCreateFile(struct inotify_event *event);
  void			logDeleteFile(struct inotify_event *event);
  void			logModifyFile(struct inotify_event *event);
  void			logOpenFile(struct inotify_event *event);
  void			logAccesFile(struct inotify_event *event);

public:
  void			logEndReceived(const std::string &status);
  void			logCmdUser(const std::string &command);
  void			logCmdUser(const std::string &command, const std::string &status);

public:
  void			logServerAction(const std::string &action, const std::string &status);
  void			logServerAddFile(const std::string &file);
  void			logServerRead(const std::string &status, const std::string &error);
  void			logServer(const std::string &msg, const std::string &status);
  void			logClientSendPacket(const std::string &file);
  void			logClient(const std::string &msg, const std::string &status);

};

#endif /* __LOGMANAGER__ */
