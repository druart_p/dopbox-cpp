#ifndef DATABASETYPE_H_
#define DATABASETYPE_H_

#include <string>

namespace db {
  
  template<typename _DataBaseType>
  class DataBaseType 
  {
  private:
    _DataBaseType *__db;
  public:
    virtual ~DataBaseType();
    
    DataBaseType();
    
    DataBaseType(const std::string & database)	
    {
      __db = new _DataBaseType(database);
    }
    
    DataBaseType(const std::string & user, 
		 const std::string & password, 
		 const std::string & database)	
    {
      __db = new _DataBaseType(user, password, database);
    }
    
    const std::string getUser(const std::string & login, const std::string & password)	
    {
      return __db->getUser(login, password);
    }
  };
  
  template<class _DataBaseType>
  DataBaseType<_DataBaseType>::DataBaseType() {}
  
  template<class _DataBaseType>
  DataBaseType<_DataBaseType>::~DataBaseType() {}

} /* namespace db */

#endif /* DATABASETYPE_H_ */
