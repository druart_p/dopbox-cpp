#include "DataBaseType.hh"

template<class _DataBaseType>
DataBaseType<_DataBaseType>::DataBaseType(const std::string &database)
{
    this->_db = new _DataBaseType(database);
}

template<class _DataBaseType>
DataBaseType<_DataBaseType>::DataBaseType(const std::string &user, 
					  const std::string &password, 
					  const std::string &database)
{
    this->_db = new _DataBaseType(user, password, database);
}

template<class _DataBaseType>
const std::string DataBaseType<_DataBaseType>::getUser(const std::string & login, const std::string & password)	
{
	return this->db->getUser(login, password);
}

template<class _DataBaseType>
DataBaseType<_DataBaseType>::DataBaseType()
{

}

template<class _DataBaseType>
DataBaseType<_DataBaseType>::~DataBaseType()
{

}
