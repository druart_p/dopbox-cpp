#include <string>
#include <fstream>

#include "Info.hpp"

int		main()
{
  std::string	pathInfo("/tmp/info.xml");
  std::ofstream	ofs(pathInfo.c_str());
  
  const Info info("Dupond", "Pierre");
  {
    boost::archive::xml_oarchive oa(ofs);
    oa << boost::serialization::make_nvp("options", info);
  }

  return 0;
}
