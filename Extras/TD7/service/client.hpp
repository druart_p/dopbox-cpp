/*
 *	client.hpp
 *
 *  Created on: 28 oct. 2014
 *      Author: nicolas
 */

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <iostream>
#include "service/connection.hpp"	/* serialisation */
#include <boost/serialization/vector.hpp>
#include "model/info.hpp"

#include <iostream>
#include <chrono>
#include <ctime>

namespace mycloud {
namespace service{

enum niveau_log { CRITICAL, ERROR, DEBUG, INFO };

class client
{
public:
	// constructeur
	client(const std::vector<mycloud::model::info> & infos, boost::asio::io_service& io_service, const std::string& host, const std::string& service)
: connection_(io_service)	{
		// resoudre l'adresse
		boost::asio::ip::tcp::resolver resolver(io_service);
		boost::asio::ip::tcp::resolver::query query(host, service);
		boost::asio::ip::tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
		// connection au serveur
		boost::asio::async_connect(connection_.socket(), endpoint_iterator, boost::bind(&client::handle_connect, this, boost::asio::placeholders::error, infos));
	}
	// handle appelé après connection
	void handle_connect(const boost::system::error_code& e, const std::vector<mycloud::model::info> & infos)	{
		if (!e)	{	// connection réussie
			// envoye des informations
			start = std::chrono::system_clock::now();
			connection_.async_write(infos, boost::bind(&client::handle_write, this, boost::asio::placeholders::error));
		} else {	// echec
			std::cerr << e.message() << std::endl;
		}
	}
	// handle appelé après émission
	void handle_write(const boost::system::error_code& e)
	{
		boost::array<char, 1909814> buf;
		if (!e) {
			while (1)	{
				boost::system::error_code error;

				int len = connection_.socket().read_some(boost::asio::buffer(buf), error);

				if (error == boost::asio::error::eof)
				{
					std::cout << "Fin de la reception des données" << std::endl;
					break;
				}
				// affichage
				std::cout.write(buf.data(), len);

				end = std::chrono::system_clock::now();
				int elapsed_seconds = std::chrono::duration_cast<std::chrono::seconds>(end-start).count();

				if (niveauLog==niveau_log::DEBUG)
				std::cout << "temps de traitement C/S " << elapsed_seconds << "s\n";

			}
		} else {
			std::cerr << e.message() << std::endl;
		}
	}
private:
	// connection au serveur
	connection connection_;
	std::chrono::time_point<std::chrono::system_clock> start, end;
	niveau_log niveauLog=niveau_log::DEBUG;
};

}

}
