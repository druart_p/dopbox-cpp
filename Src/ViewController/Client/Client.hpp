#ifndef __CLIENT_HPP__
#define __CLIENT_HPP__

#include <boost/algorithm/string.hpp>
#include <boost/thread.hpp>

/* SYSTEM */
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <ctime>

/* DATABASE */
#include "./../../../Includes/database/DataBaseType.hpp"
#include "./../../../Includes/database/MySQL.hpp"
#include "./../../../Includes/database/SQLite.hpp"

/* MANAGER */
#include "./../../Manager/Stream/StreamManager.hpp"

/* SERVICE */
#include "./../../Service/Notify/Notify.hpp"
#include "./../../Service/Client/CommunicationClient.hpp"


/* UTILS */
#include "./../../Utils/Constante.h"

class Client
{
private:
  StreamManager				       	*__streamManager;
  LogManager				       	*__logManager;
  Notify					*__notify;
  boost::thread					*__notifyThread;

private:
  std::string	       				__id;
  std::string	       				__lastCmd;
  std::string					__rootLogin;
  std::string					__rootPassword;
  std::string					__database;
  std::string					__home;
  std::string					__login;
  bool						__isLogin;
  bool						__isNotify;

private:
  typedef void (Client:: *Function)();
  std::map<std::string, Function>	       	__func;
  
public:
  Client(StreamManager *streamManager, const std::string &home);
  ~Client();

private:
  void				initEventParser();
  void				clientEventListener();
  void				getMyCloudAccesConfig();
  void				eventCmdNotFound(const std::string &cmd);
  bool				checkUserLogin();
  db::DataBaseType<db::MySQL>	*getDB();
  void				stopNotifySucces();

private:
  void				eventHelpClient();
  void				eventCreateUserClient();
  void				eventLoginClient();
  void				eventDisconnectClient();
  void				eventSynchronizeClient();
  void				eventStartNotifyClient();
  void				eventStopNotifyClient();
  void				eventStatusClient();
  void				eventLogClient();
  void				eventExitClient();

private:
  bool				matchUserDB(const std::string &user, const std::string &password); 

private:
  void				launchNotifyThread();

private:
  void				loginClient(int tryPassword);
  void				disconnectClient();
  void				createUserClient();
  void				startNotifyClient();
  void				stopNotifyClient();
  void				getLogFromTrace();
};

#endif /* __CLIENT__ */
