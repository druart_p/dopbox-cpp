#ifndef __CONSTANTE_HPP__
#define __CONSTANTE_HPP__

#include <string>

/* MYCLOUD TYPE EXECUTION */
static const std::string client = "Client"; 
static const std::string server = "Server";

/* MYCLOUD CONNECTION */
static const std::string IP_SERVER	= "127.0.0.1";
static const std::string DATABASE	= "mycloud";
static const std::string PORT_CLIENT	= "5554";
static const unsigned short portServer	= 5554;

#define TRY_PASSWORD	3	// error accept to connect
#define TIME_REFRESH	3	// in seconds
#define LOG_LINE	20	// line client log

/* MYCLOUD PATH */
static const std::string MYCLOUD_CONFIG = "./.MyCloud/MyCloud.config";
static const std::string TRACE_SERVER	= "./.MyCloud/MyCloud.server";
static const std::string TRACE_CLIENT	= "./.MyCloud/MyCloud.client";
static const std::string XML_SERVER	= "/.MyCloud/referentiel_server.xml";
static const std::string XML_CLIENT	= "/.MyCloud/referentiel_client_";
static const std::string PATH_SERVER	= "/Server/";
static const std::string PATH_CLIENT	= "/Client/";

/* NOTIFY ACTION */
static const std::string MOVED_TO	= "IN MOVE TO";
static const std::string MOVED_FROM	= "IN MOVE FROM";
static const std::string DELETE		= "IN DELETE";
static const std::string MODIFY		= "IN MODIFY";
static const std::string CREATE		= "IN CREATE";
static const std::string REFERENTIEL   	= "REFERENTIEL";

/* DEFINE COLOR */
#define RED		"\033[0;31m"
#define GREEN		"\033[0;32m"
#define YELLOW		"\033[0;33m"
#define BLUE		"\033[0;34m"
#define MAGENTA		"\033[0;35m"
#define CYAN		"\033[0;36m"
#define NCOLOR		"\033[0m"

/* DEFINE NOTIFY */
#define MAX_EVENTS	1024
#define BUF_LEN		(MAX_EVENTS * (sizeof (struct inotify_event) + NAME_MAX + 1))

#endif /* __CONSTANTE__ */
