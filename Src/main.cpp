#include <cstdlib>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>

#include "./Utils/Constante.h"
#include "./Utils/Exception.hpp"

#include "MyCloud.hpp"
#include "./Service/Notify/Notify.hpp"

void handlerSignal(int signal)
{
  std::cout << std::endl << "Type 'Exit' to quit properly." << std::endl;
}

int	main(int ac, char **av)
{
  try
    { 
      signal(SIGINT, handlerSignal);

      if (ac == 2)
	{
	  MyCloud *__myCloud;
	  
	  if (!client.compare(av[1]))
	    {
	      __myCloud = new MyCloud(av[1]);
	    }
	  else if (!server.compare(av[1]))
	    {
	      __myCloud = new MyCloud(av[1]);
	    }
	  else
	    throw my_exception_argument_type(av[1]);	  
	}
      else
	throw my_exception_argument_number(ac);
    }
  catch (const std::exception &e)
    {
      std::cerr << e.what() << std::endl;
    }
  
  return 0;
}
